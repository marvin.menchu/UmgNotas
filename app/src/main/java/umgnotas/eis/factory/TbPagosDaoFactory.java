/*
 * This source file was generated by FireStorm/DAO.
 * 
 * If you purchase a full license for FireStorm/DAO you can customize this header file.
 * 
 * For more information please visit http://www.codefutures.com/products/firestorm
 */

package umgnotas.eis.factory;

import java.sql.Connection;
import umgnotas.eis.dao.*;
import umgnotas.eis.jdbc.*;

public class TbPagosDaoFactory
{
	/**
	 * Method 'create'
	 * 
	 * @return TbPagosDao
	 */
	public static TbPagosDao create()
	{
		return new TbPagosDaoImpl();
	}

	/**
	 * Method 'create'
	 * 
	 * @param conn
	 * @return TbPagosDao
	 */
	public static TbPagosDao create(Connection conn)
	{
		return new TbPagosDaoImpl( conn );
	}

}
