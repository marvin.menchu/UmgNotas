/*
 * This source file was generated by FireStorm/DAO.
 * 
 * If you purchase a full license for FireStorm/DAO you can customize this header file.
 * 
 * For more information please visit http://www.codefutures.com/products/firestorm
 */

package umgnotas.eis.exceptions;

public class TbCarreraDaoException extends DaoException
{
	/**
	 * Method 'TbCarreraDaoException'
	 * 
	 * @param message
	 */
	public TbCarreraDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'TbCarreraDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public TbCarreraDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
