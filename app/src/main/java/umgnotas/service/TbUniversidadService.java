package umgnotas.service;

import umgnotas.eis.dto.TbUniversidad;

/**
 * Created by marvinmanuelmenchumenchu on 25/07/17.
 */

public interface TbUniversidadService {

    public boolean guardarUniversidad(TbUniversidad tbUniversidad);

    public boolean verificarUniversidad ();

}
