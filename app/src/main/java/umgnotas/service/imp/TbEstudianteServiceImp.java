package umgnotas.service.imp;

import com.umg.maestria.umgnotas.Estudiantes;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import umgnotas.eis.dao.TbEstudianteDao;
import umgnotas.eis.dto.TbEstudiante;
import umgnotas.eis.exceptions.TbEstudianteDaoException;
import umgnotas.eis.factory.TbEstudianteDaoFactory;
import umgnotas.eis.jdbc.ResourceManager;
import umgnotas.service.TbEstudianteService;
import umgnotas.service.exceptions.BusinessException;

/**
 * Created by marvinmanuelmenchumenchu on 28/07/17.
 */

public class TbEstudianteServiceImp implements TbEstudianteService {

    private static TbEstudianteService tbEstudianteServiceInstance;
    TbEstudianteDao tbEstudianteDao;


    private TbEstudianteServiceImp() {

    }

    public static TbEstudianteService getInstance() {
        if (tbEstudianteServiceInstance == null) {
            tbEstudianteServiceInstance = new TbEstudianteServiceImp();
        }
        return tbEstudianteServiceInstance;
    }

    @Override
    public List<TbEstudiante> listaDeEstudiantes() {
        try {
            this.tbEstudianteDao = TbEstudianteDaoFactory.create();
            return Arrays.asList(this.tbEstudianteDao.findAll());
        } catch (TbEstudianteDaoException ex) {
            throw new BusinessException("Existe un problema al obtener el arreglo Estudiantes en la BD", ex);
        }
    }

    @Override
    public boolean guardarEstudiante(TbEstudiante tbEstudiante) {
        Connection conn = null;
        try {
            this.tbEstudianteDao = TbEstudianteDaoFactory.create();
            conn = ResourceManager.getConnection();
            conn.setAutoCommit(false);
            this.tbEstudianteDao.setUserConn(conn);
                this.tbEstudianteDao.insert(tbEstudiante);
            conn.commit();
            return true;
        } catch (TbEstudianteDaoException ex) {
            try {
                conn.rollback();
            } catch (SQLException ex1) {
                throw new BusinessException("No se pudo reestablecer el estado de la Base de Datos", ex1);
            }
            throw new BusinessException("No se puedo agregar estudiantes: " + tbEstudiante + " a la BD", ex);
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException ex1) {
                throw new BusinessException("No se pudo reestablecer el estado de la Base de Datos", ex1);
            }
            throw new BusinessException("Existe un problema con la Base de Datos", ex);
        } finally {
            ResourceManager.close(conn);
        }
    }
}
