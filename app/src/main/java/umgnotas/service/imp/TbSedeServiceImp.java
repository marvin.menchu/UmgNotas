package umgnotas.service.imp;

import java.sql.Connection;
import java.sql.SQLException;

import umgnotas.eis.dao.TbSedeDao;
import umgnotas.eis.dto.TbSede;
import umgnotas.eis.dto.TbSedePk;
import umgnotas.eis.exceptions.TbSedeDaoException;
import umgnotas.eis.factory.TbSedeDaoFactory;
import umgnotas.eis.jdbc.ResourceManager;
import umgnotas.service.TbSedeService;
import umgnotas.service.exceptions.BusinessException;

/**
 * Created by marvinmanuelmenchumenchu on 26/07/17.
 */

public class TbSedeServiceImp implements TbSedeService {

    private static TbSedeService tbSedeServiceInstance;
    TbSedeDao tbSedeDao;


    private TbSedeServiceImp() {

    }

    public static TbSedeService getInstance() {
        if (tbSedeServiceInstance == null) {
            tbSedeServiceInstance = new TbSedeServiceImp();
        }
        return tbSedeServiceInstance;
    }



    @Override
    public boolean guardarSede(TbSede tbSede) {
        Connection conn = null;
        TbSedePk codigoSede;
        try {
            this.tbSedeDao = TbSedeDaoFactory.create();
            conn = ResourceManager.getConnection();
            conn.setAutoCommit(false);
            this.tbSedeDao.setUserConn(conn);
            if (tbSede.getSedCodigo() == null) {
                this.tbSedeDao.insert(tbSede);
            } else {
                this.tbSedeDao.update(tbSede.createPk(), tbSede);
            }
            conn.commit();
            return true;
        } catch (TbSedeDaoException ex) {
            try {
                conn.rollback();
            } catch (SQLException ex1) {
                throw new BusinessException("No se pudo reestablecer el estado de la Base de Datos", ex1);
            }
            throw new BusinessException("No se puedo agregar sedes: " + tbSede + " a la BD", ex);
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException ex1) {
                throw new BusinessException("No se pudo reestablecer el estado de la Base de Datos", ex1);
            }
            throw new BusinessException("Existe un problema con la Base de Datos", ex);
        } finally {
            ResourceManager.close(conn);
        }
    }

    @Override
    public boolean verificarSede() {
        try {
            this.tbSedeDao = TbSedeDaoFactory.create();
            final String SQL_WHERE = " SED_CODIGO = ? ";
            Object[] sqlParams = {1};
            TbSede[] sede = this.tbSedeDao.findByDynamicWhere(SQL_WHERE, sqlParams);
            if (sede.length > 0) {
                return true;
            }
        } catch (TbSedeDaoException ex) {
            throw new BusinessException("Existe un problema al obtener la sede en la BD", ex);
        }
        return false;
    }
}
