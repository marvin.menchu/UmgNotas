package umgnotas.service.imp;

import umgnotas.eis.dao.TbTransaccionDao;
import umgnotas.service.TbTransaccionService;

/**
 * Created by marvinmanuelmenchumenchu on 28/07/17.
 */

public class TbTransaccionServiceImp implements TbTransaccionService {

    private static TbTransaccionService tbTransaccionServiceInstance;
    TbTransaccionDao tbTransaccionDao;


    private TbTransaccionServiceImp() {

    }

    public static TbTransaccionService getInstance() {
        if (tbTransaccionServiceInstance == null) {
            tbTransaccionServiceInstance = new TbTransaccionServiceImp();
        }
        return tbTransaccionServiceInstance;
    }
}
