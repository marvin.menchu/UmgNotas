package umgnotas.service.imp;

import umgnotas.eis.dao.TbAsignacionDao;
import umgnotas.eis.dto.TbAsignacion;
import umgnotas.service.TbAsignacionService;

/**
 * Created by marvinmanuelmenchumenchu on 28/07/17.
 */

public class TbAsignacionServiceImp implements TbAsignacionService {

    private static TbAsignacionService tbAsignacionServiceInstance;
    TbAsignacionDao tbAsignacionDao;


    private TbAsignacionServiceImp() {

    }

    public static TbAsignacionService getInstance() {
        if (tbAsignacionServiceInstance == null) {
            tbAsignacionServiceInstance = new TbAsignacionServiceImp();
        }
        return tbAsignacionServiceInstance;
    }
}
