package umgnotas.service.imp;

import umgnotas.eis.dao.TbSemestreDao;
import umgnotas.service.TbSemestreService;

/**
 * Created by marvinmanuelmenchumenchu on 28/07/17.
 */

public class TbSemestreServiceImp implements TbSemestreService{

    private static TbSemestreService tbSemestreServiceInstance;
    TbSemestreDao tbUsuarioDao;


    private TbSemestreServiceImp() {

    }

    public static TbSemestreService getInstance() {
        if (tbSemestreServiceInstance == null) {
            tbSemestreServiceInstance = new TbSemestreServiceImp();
        }
        return tbSemestreServiceInstance;
    }
}
