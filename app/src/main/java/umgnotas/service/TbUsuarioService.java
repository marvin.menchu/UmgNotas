package umgnotas.service;

import umgnotas.eis.dto.TbUsuario;

/**
 * Created by marvinmanuelmenchumenchu on 25/07/17.
 */

public interface TbUsuarioService {

    public boolean guardarUsuario(TbUsuario tbUsuario);

    public boolean existeUsuario();

    public boolean validarUsuario(TbUsuario tbUsuario);
}
