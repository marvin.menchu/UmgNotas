package umgnotas.service;

import com.umg.maestria.umgnotas.Estudiantes;

import java.util.List;

import umgnotas.eis.dto.TbEstudiante;

/**
 * Created by marvinmanuelmenchumenchu on 25/07/17.
 */

public interface TbEstudianteService {

    public List<TbEstudiante> listaDeEstudiantes();

    public boolean guardarEstudiante(TbEstudiante tbEstudiante);



}
