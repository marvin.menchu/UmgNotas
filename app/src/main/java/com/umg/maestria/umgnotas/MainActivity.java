package com.umg.maestria.umgnotas;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import umgnotas.eis.dto.TbUniversidad;
import umgnotas.eis.dto.TbUsuario;
import umgnotas.service.TbSedeService;
import umgnotas.service.TbUniversidadService;
import umgnotas.service.TbUsuarioService;
import umgnotas.service.imp.TbSedeServiceImp;
import umgnotas.service.imp.TbUniversidadServiceImp;
import umgnotas.service.imp.TbUsuarioServiceImp;

public class MainActivity extends AppCompatActivity {

    private EditText user, pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Intent uniIntent = new Intent(this, Universidad.class);
        Intent sedIntent = new Intent(this, Sede.class);
        Intent useIntent = new Intent(this, UserAdmin.class);

        //verificamos si existe una registro en universidad
        TbUniversidadService tbUniversidadService = TbUniversidadServiceImp.getInstance();
        boolean existeUni = tbUniversidadService.verificarUniversidad();

        //verificamos si existe una sede
        TbSedeService tbSedeService = TbSedeServiceImp.getInstance();
        boolean existeSede = tbSedeService.verificarSede();

        //verificamos si existe usuario admin en la base de datos
        TbUsuarioService tbUsuarioService = TbUsuarioServiceImp.getInstance();
        boolean existeUser = tbUsuarioService.existeUsuario();

        if (!existeUni){
            finish();
            startActivity(uniIntent);
        }else if (!existeSede){
            finish();
            startActivity(sedIntent);
        }else if (!existeUser){
            finish();
            startActivity(useIntent);
        }else{
            setContentView(R.layout.activity_main);
            user = (EditText) findViewById(R.id.etUser);
            pass = (EditText) findViewById(R.id.etPass);
        }


    }

    public void login(View view){
        String usuario = user.getText().toString();
        String password = pass.getText().toString();

        //creamos un objeto Tbusuario
        TbUsuario tbUsuario = new TbUsuario();
        tbUsuario.setUsuCodigo(usuario);
        tbUsuario.setUsuPassword(MD5(password));

        //instanciamos los servicios
        TbUsuarioService tbUsuarioService = TbUsuarioServiceImp.getInstance();
        boolean existe = tbUsuarioService.validarUsuario(tbUsuario);
        if(existe){
            Intent e = new Intent(this, Escritorio.class);
            e.putExtra("usuario", usuario);
            startActivity(e);
            finish();
            Toast notification = Toast.makeText(this, "Bienvenido", Toast.LENGTH_LONG);
            notification.show();
        }else{
            Toast notification = Toast.makeText(this, "Ups! Usuario y/o contraseña incorrecta!", Toast.LENGTH_LONG);
            notification.show();
        }



    }

    public String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }
}
