package com.umg.maestria.umgnotas;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import umgnotas.eis.dto.TbEstudiante;
import umgnotas.service.TbEstudianteService;
import umgnotas.service.TbUsuarioService;
import umgnotas.service.imp.TbEstudianteServiceImp;
import umgnotas.service.imp.TbUsuarioServiceImp;

public class EditarEstudiante extends AppCompatActivity {

    private DatePicker datePicker;
    private Calendar calendar;
    private TextView dateView;
    private int year, month, day;

    private EditText carne;
    private EditText pnombre;
    private EditText snombre;
    private EditText papellido;
    private EditText sapellido;
    private TextView fechaNac;
    private RadioButton sexoM;
    private RadioButton sexoF;
    private EditText telefono;

    private String sexoSeleccionado;
    private String usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_estudiante);

        usuario = getIntent().getStringExtra("usuario");

        dateView = (TextView) findViewById(R.id.textView3);
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(year, month+1, day);

        carne = (EditText) findViewById(R.id.carne);
        pnombre = (EditText) findViewById(R.id.pnombre);
        snombre = (EditText) findViewById(R.id.snombre);
        papellido = (EditText) findViewById(R.id.papellido);
        sapellido = (EditText) findViewById(R.id.sapellido);
        fechaNac = (TextView) findViewById(R.id.textView3);
        sexoF = (RadioButton) findViewById(R.id.sexoF);
        sexoM = (RadioButton) findViewById(R.id.sexoM);
        telefono = (EditText) findViewById(R.id.telefono);



    }

    public void guardarEstudiante(View view){
        String car = carne.getText().toString();
        String pno = pnombre.getText().toString();
        String sno = snombre.getText().toString();
        String pap = papellido.getText().toString();
        String sap = sapellido.getText().toString();
        String fec = fechaNac.getText().toString();
        String sex = sexoSeleccionado;
        String tel = telefono.getText().toString();

        //Creamos un objeto estudiante
        TbEstudiante tbEstudiante = new TbEstudiante();
        tbEstudiante.setEstCarne(car);
        tbEstudiante.setEstPrimerNombre(pno);
        tbEstudiante.setEstSegundoNombre(sno);
        tbEstudiante.setEstPrimerApellido(pap);
        tbEstudiante.setEstSegundoApellido(sap);

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        Date date = new Date();
        try {

            date = formatter.parse(fec);
            System.out.println(date);
            System.out.println(formatter.format(date));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        tbEstudiante.setEstFechaNacimiento(date);
        tbEstudiante.setEstSexo(sex);
        tbEstudiante.setEstTelefono(tel);
        tbEstudiante.setEstEstado("A");
        tbEstudiante.setEstFechaIngreso(new Date());
        tbEstudiante.setEstUsuIngreso(usuario);
        tbEstudiante.setEstSedCodigo(1);

        //llamamos los servicios

        TbEstudianteService tbEstudianteService = TbEstudianteServiceImp.getInstance();
        boolean guardado = tbEstudianteService.guardarEstudiante(tbEstudiante);
        if (guardado){
            Intent ua = new Intent(this, Estudiantes.class);
            ua.putExtra("usuario", usuario);
            startActivity(ua);
            this.enviarMensaje(tel, "Hola " + pnombre.getText().toString() + " " + papellido.getText().toString() + "! Nos da gusto que estudies en nuestra Universidad. Bienvenid"+ (sex.equals("M")?"o":"a") +" sus datos han sido agregados. Su carne: "+ carne.getText().toString());
            Toast notification = Toast.makeText(this, "Estudiante guardado exitosamente!", Toast.LENGTH_LONG);
            notification.show();
            finish();
        }else{
            Toast notification = Toast.makeText(this, "Ups, ocurrio un error, intente de nuevo!", Toast.LENGTH_LONG);
            notification.show();
        }

    }


    public void onRadioButtonClicked(View view){
        boolean checked = ((RadioButton) view).isChecked();

        switch(view.getId()){
            case R.id.sexoM:
                if(checked)
                    sexoSeleccionado = "M";
                break;
            case R.id.sexoF:
                if(checked)
                    sexoSeleccionado = "F";
                break;
        }
    }

    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        showDialog(999);
        Toast.makeText(getApplicationContext(), "ca", Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this, myDateListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
            // arg1 = year
            // arg2 = month
            // arg3 = day
            showDate(arg1, arg2+1, arg3);
        }
    };

    private void showDate(int year, int month, int day) {
        dateView.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
    }

    private void enviarMensaje (String Numero, String Mensaje){
        try {
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage(Numero,null,Mensaje,null,null);
            Toast.makeText(getApplicationContext(), "Mensaje Enviado.", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Mensaje no enviado, datos incorrectos.", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

    }

}
