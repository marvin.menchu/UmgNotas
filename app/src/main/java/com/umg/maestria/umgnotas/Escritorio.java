package com.umg.maestria.umgnotas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class Escritorio extends AppCompatActivity {

    private TextView tv1;
    private String usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escritorio);

        usuario = getIntent().getStringExtra("usuario");

        tv1 = (TextView) findViewById(R.id.textoBienvenida);
        tv1.setText("Bienvenid@ "+ usuario);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_escritorio, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.datosUniversidad) {
            Intent datosUniversidad = new Intent(getApplicationContext(), DatosUniversidad.class);
            datosUniversidad.putExtra("usuario", usuario);
            startActivity(datosUniversidad);
            return true;
        }

        if (id == R.id.estudiantes) {
            Intent e = new Intent(getApplicationContext(), Estudiantes.class);
            e.putExtra("usuario", usuario);
            startActivity(e);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
