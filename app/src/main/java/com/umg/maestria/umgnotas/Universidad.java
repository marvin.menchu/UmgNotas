package com.umg.maestria.umgnotas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import umgnotas.eis.dto.TbUniversidad;
import umgnotas.eis.jdbc.TbUniversidadDaoImpl;
import umgnotas.service.TbUniversidadService;
import umgnotas.service.imp.TbUniversidadServiceImp;

public class Universidad extends AppCompatActivity {

    private EditText uNom;
    private EditText uDir;
    private EditText uTel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_universidad);

        uNom = (EditText) findViewById(R.id.nomUni);
        uDir = (EditText) findViewById(R.id.dirUni);
        uTel = (EditText) findViewById(R.id.telUni);

    }

    public void guardarUniversidad(View view){

        String nombreU = uNom.getText().toString();
        String direccionU = uDir.getText().toString();
        String telefonoU = uTel.getText().toString();

        //creo un nuevo objeto universidad
        TbUniversidad tbUniversidad = new TbUniversidad();
        tbUniversidad.setUniCodigo(null);
        tbUniversidad.setUniNombre(nombreU);
        tbUniversidad.setUniDireccion(direccionU);
        tbUniversidad.setUniTelefono(telefonoU);

        //llamo los servicios para guardar los datos de la universidad
        TbUniversidadService tbUniversidadService = TbUniversidadServiceImp.getInstance();
        boolean guardado = tbUniversidadService.guardarUniversidad(tbUniversidad);

        if (guardado){
            finish();
            Intent s = new Intent(this, Sede.class);
            startActivity(s);
            Toast notification = Toast.makeText(this, "Universidad guardada exitosamente!", Toast.LENGTH_LONG);
            notification.show();
        }else{
            Toast notification = Toast.makeText(this, "Ups, ocurrio un error, intente de nuevo!", Toast.LENGTH_LONG);
            notification.show();
        }
    }

}
