package com.umg.maestria.umgnotas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import umgnotas.eis.dto.TbEstudiante;
import umgnotas.service.TbEstudianteService;
import umgnotas.service.imp.TbEstudianteServiceImp;

public class Estudiantes extends AppCompatActivity {

    private ListView lv1;
    private List<String> listado;
    private ArrayAdapter<String> adapter;

    private String usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estudiantes);

        lv1 = (ListView) findViewById(R.id.listaEstudiantes);

        usuario = getIntent().getStringExtra("usuario");

        listado=new ArrayList<>();
        //llamo los servicios de estudiantes
        TbEstudianteService tbEstudianteService = TbEstudianteServiceImp.getInstance();
        List<TbEstudiante> listaEstudiante = tbEstudianteService.listaDeEstudiantes();

        if (!listaEstudiante.isEmpty()){
            for(TbEstudiante est : listaEstudiante){
                listado.add(est.toString());
            }

            adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,listado);
            lv1.setAdapter(adapter);
        }



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_estudiante, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.adicionarEstudiante) {
            Intent ae = new Intent(getApplicationContext(), EditarEstudiante.class);
            ae.putExtra("usuario", usuario);
            startActivity(ae);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
